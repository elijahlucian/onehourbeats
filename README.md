# One Hour Competition App!

# Milestones

### MVP

- [] user login
- [] create session
- [] upload jam entries
- [x] timer
- [] dashbaord
- [x] dashbaord sidebar
- [x] routing
- [x] basic data structures in TS
- [x] basic randomizer

### milestone 1

- add your own content to the randomizers
- add content
- request new section
- analytics on button clicks
- share prompt button
- Email verified users only (or SSO)

### milestone 2

- private Rooms.
- user roles
- user submitted data
- upvote for best data

### milestone 3

- notifiy users of time remaining
- competition entry upvoting
- sharing contests on socials
- upload to soundcloud / bandcamp etc

### milestone 4

- sample packs
- integrate with splice or some shit

### milestone 5

- profit

## TODO

- TSify as you go

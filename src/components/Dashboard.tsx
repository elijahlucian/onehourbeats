import React from 'react';
import { User } from '../types/schema';

type Props = {
  user: User;
};

export const Dashboard: React.FC<Props> = ({ user }) => {
  return (
    <div>
      <h1>Welcome Back, {user.username}</h1>
      <h3>News</h3>
      <h3>Schedules Jams</h3>
      <ul>
        <li>
          <button>Join!</button>
        </li>
      </ul>
      <h3>Scheduled Jams</h3>
      <h3>Global Stats</h3>
      <ul>
        <li>Your teams</li>
        <li>Inspiration: https://splice.com/studio</li>
        <li>
          Other Fake Name Options -
          https://www.npmjs.com/package/unique-names-generator
        </li>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://gitlab.com/elijahlucian/onehourbeats/-/boards"
        >
          Go To Project Board
        </a>
      </ul>
    </div>
  );
};

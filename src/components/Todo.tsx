import React from 'react';
import './css/index.css';

const Todo = () => {
  return (
    <div className="todo">
      <h2>TODO Milestone 1 - inspiration app</h2>
      <ul>
        <li>randomizer</li>
        <li>add to data resources</li>
        <li>randomize all button</li>
        <li>analytics on button clicks</li>
        <li>share prompt button</li>
      </ul>
      <h2>TODO Milestone 2 - adding more inspiration!</h2>
      <ul>
        <li>user login</li>
        <li>sqlite database</li>
        <li>orm?</li>
        <li>upvote and downvote data resources</li>
        <li>sort data resources</li>
      </ul>
      <h2>TODO Milestone 3 - One Hour Competition App!</h2>
      <ul>
        <li>start timer</li>
        <li>file upload</li>
        <li>notify users</li>
      </ul>
    </div>
  );
};

export default Todo;

import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { useParams } from 'react-router-dom';
import _ from 'lodash';

import { Session, Prompt, Submission } from '../types/schema';

import './css/session.css';
import { PromptView } from './common/PromptView';
import { Timer } from './Timer';
import { SubmissionPlayer } from './common/SubmissionPlayer';
import { promptIndex } from '../mock_db/prompts';
import { submissionIndex } from '../mock_db/submissions';
import { sessionIndex } from '../mock_db/sessions';
// Need all members that have joined the jam

type Props = {};

export const SessionView: React.FC<Props> = () => {
  let { id } = useParams();

  const [session, setSession] = useState<Session | null>(null);
  const [prompts, setPrompts] = useState<Prompt[]>([]);
  const [submissions, setSubmissions] = useState<Submission[]>([]);

  useEffect(() => {
    setSession(_.find(sessionIndex, session => session.id === id) || null);
  }, [id]);

  useEffect(() => {
    if (!session) return;
    setPrompts(
      _.filter(promptIndex, prompt => session.prompts.includes(prompt.id)) ||
        [],
    );
    setSubmissions(
      _.filter(submissionIndex, submission =>
        session.submissions.includes(session.id),
      ) || [],
    );
  }, [session]);

  if (session) document.title = `One Hour Beats - Session: ${session.id}`;

  return !session ? (
    <div>Loading...</div>
  ) : (
    <div className="session">
      <h3>One Hour Beat Session: {session.id}</h3>
      <div className="session-settings">
        <h4>Session Settings</h4>
        <p>can do a kind of table view here? or a top bar style settings.</p>
        <div>
          Created At: {moment(session.created_at).format('MM-DD-YY hh:mm')}
        </div>
        <div>Timer Length: {session.timer_length / 60} minutes</div>
        <div>
          Milestone 2: password: {session.password || 'not password protected'}
        </div>
        <div>
          Milestone 1: verified users only:
          {session.verified_only ? 'YES' : 'NO'}
        </div>
      </div>

      <div>
        <h5>Prompts</h5>
        <div>find a fun way to show the prompts</div>
        <div>maybe card style would be cool</div>
        {prompts.map(prompt => (
          <PromptView key={prompt.id} {...prompt} />
        ))}
      </div>

      {session.timer_started_at && session.timer_started_at > 0 && (
        <Timer
          length={session.timer_length}
          started_at={session.timer_started_at}
        />
      )}

      {session.timer_started_at && session.timer_started_at > 0 ? (
        <button className="submission-button">Submit Your Jam Entry!</button>
      ) : (
        <div>
          <h3>Submissions are Closed!</h3>
          <button>Submit - Post Jam Entry</button>
        </div>
      )}

      <div className="session-submissions">
        <h3>Submissions</h3>
        <p>users should not be able to listen to these until it's over</p>
        {submissions.map((submission, i) => (
          <SubmissionPlayer userId={submission.user_id} url={submission.url} />
        ))}
      </div>
    </div>
  );
};

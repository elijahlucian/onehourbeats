import React from 'react';

type Props = {
  url: string;
  userId: string;
};

export const SubmissionPlayer: React.FC<Props> = ({ userId, url }) => {
  return (
    <div>
      <p>{userId}</p>
      <audio
        className="submission-audio"
        controls
        src={`/submission_files/userId/${url}`}
      />
    </div>
  );
};

import React, { useState, useEffect } from 'react';
import { HotInput } from './HotThings';
import { Prompt } from '../../types/schema';

import data from '../../data/data';
// import { Random } from 'random-js';
const uuidv1 = require('uuid/v1');

// const random = new Random();
// random.bool();

type Props = {
  setter: (prompt: Prompt) => void;
};

export const PromptMaker: React.FC<Props> = ({ setter }) => {
  const [typeIndex, setTypeIndex] = useState(0);
  const [type, setType] = useState<string>(data.keys[typeIndex]);

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setType(e.target.value);
  };

  const addRandomPrompt = () => {
    console.log('addingrandom prompt');
  };

  const packagePrompt = (description: string) => {
    if (!type || !description) {
      console.log('Missing stuff:', type, description);
      return;
    }
    setter({ id: uuidv1(), description, type });

    // TODO: if CTRL key down, don't increment.

    if (typeIndex < data.keys.length) {
      setTypeIndex(typeIndex + 1);
    } else {
      setTypeIndex(0);
    }
  };

  useEffect(() => {
    setType(data.keys[typeIndex]);
  }, [typeIndex]);

  return (
    <>
      <select onChange={handleChange} value={type}>
        <option disabled value="disabled">
          Select A Prompt Category
        </option>
        {data.keys.map((item, i) => (
          <option key={i} value={item}>
            {item}
          </option>
        ))}
      </select>
      <HotInput setter={packagePrompt} label={'write a prompt >'} />
      <button onClick={addRandomPrompt}>Add Random!</button>
    </>
  );
};

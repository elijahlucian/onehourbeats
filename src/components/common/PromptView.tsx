import React from 'react';
import { Prompt } from '../../types/schema';

export const PromptView: React.FC<Prompt> = ({ id, type, description }) => {
  return (
    <div key={id}>
      {type} - {description}
    </div>
  );
};

import React, { useState } from 'react';
import './css/hotthings.css';

type Props = {
  setter: (val: string) => void;
  label?: string;
  live?: boolean;
};

export const HotInput: React.FC<Props> = ({ setter, label, live = false }) => {
  const [text, setText] = useState('');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
    if (live) setter(e.target.value);
  };

  const handleKeyDown = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') {
      if (!text) return;
      setter(text);
      if (!live) setText('');
    }
  };

  return (
    // validate shit here
    <div className="hot-things">
      <label>
        {label}
        <input
          value={text}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
        ></input>
      </label>
    </div>
  );
};

export const HotText: React.FC<Props> = ({ setter, label, live = true }) => {
  const [text, setText] = useState('');

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value);
    if (live) setter(e.target.value);
  };

  return (
    // validate shit here
    <div className="hot-things">
      <label>{label}</label>
      <textarea value={text} onChange={handleChange}></textarea>
    </div>
  );
};

import React, { useState } from 'react';
import { RuleView } from './common/RuleView';
import { PromptView } from './common/PromptView';

import { HotInput, HotText } from './common/HotThings';
import { PromptMaker } from './common/PromptMaker';
import { Rule, Prompt } from '../types/schema';

import faker from 'faker';

export const Creator = () => {
  const [prompts, setPrompts] = useState<Prompt[]>([]);
  const [rules, setRules] = useState<Rule[]>([]);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [timer, setTimer] = useState<number | null>();

  const createSession = (e: React.MouseEvent<HTMLButtonElement>) => {
    // creates new endpoint id
    // persists session info to database
  };

  const updateName = (val: string) => {
    // custom validations
    setName(val);
  };

  const updateDescription = (val: string) => {
    // custom validations
    setDescription(val);
  };

  const generateName = () => {
    setName(faker.commerce.productName());
  };

  const updateTimer = (val: string) => {
    setTimer(parseInt(val));
  };

  const newRule = (val: string) => {
    setRules([...rules, { id: rules.length, description: val }]);
  };

  const addPrompt = (p: Prompt) => {
    setPrompts([...prompts, p]);
  };

  const addSamplePack = () => {
    // TODO: Modal to pop up and get the creator to say they have rights to upload a sample pack
    // TODO: Milestone 1
    console.log('adding sample pack');
  };

  return (
    <div className="app">
      <div className="header">
        <h1>Create New Jam</h1>
        <PromptMaker setter={addPrompt} />
        {prompts.map(prompt => (
          <PromptView key={prompt.id} {...prompt} />
        ))}
        {prompts.length > 1 && (
          <button onClick={createSession}>Create Session</button>
        )}
        <HotInput
          label="Timer Length (Seconds)"
          setter={updateTimer}
          live={true}
        />
        <HotInput label="Jam Name" setter={updateName} live={true} />
        <button onClick={generateName}>Generate</button>
        <HotText label="Description" setter={updateDescription} />
        <HotInput label="Rules" setter={newRule} />
        {rules.map(rule => (
          <RuleView key={rule.id} {...rule} />
        ))}
        <button disabled onClick={addSamplePack}>
          Add Sample Pack
        </button>
        <h1>Jam Preview</h1>
        <div>
          {name && <h3>Name: {name}</h3>}
          {timer && <h3>Timer: {timer}</h3>}
          {description && (
            <>
              <h3>Description</h3>
              <p>{description}</p>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

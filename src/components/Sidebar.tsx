import React from 'react';
import { Link } from 'react-router-dom';
import { User, Submission, UserSession } from '../types/schema';

type Props = {
  user: User;
  submissions: Submission[];
  userSessions: UserSession[];
};

export const Sidebar: React.FC<Props> = ({
  user,
  submissions = [],
  userSessions = [],
}) => {
  return (
    <div className="sidebar">
      <Link to="/submissions">
        <h3>Your Entries</h3>
      </Link>
      {submissions.map((submission, i) => (
        <div key={i}>
          <Link to={`/submissions/${submission.id}`}>{submission.title}</Link>
        </div>
      ))}
      <Link to="/sessions">
        <h3>Your Sessions</h3>
      </Link>
      {userSessions.map((session, i) => (
        <div key={i}>
          <Link to={`/sessions/${session.session_id}`}>
            {session.session_id}
          </Link>
        </div>
      ))}
    </div>
  );
};

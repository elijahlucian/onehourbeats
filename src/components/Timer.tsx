import React, { useState, useEffect } from 'react';
import moment from 'moment';

type Props = {
  started_at: number;
  length: number;
};

export const Timer: React.FC<Props> = ({ started_at, length }) => {
  const [remaining, setRemaining] = useState(length);

  // timer shit (could be in timer component)
  useEffect(() => {
    const interval = setInterval(() => {
      setRemaining(started_at + length - moment().unix());
    });
    return () => clearInterval(interval);
  }, [remaining, started_at, length]);

  const remaining_minutes = Math.floor(remaining / 60);
  const remaining_seconds = remaining % 60;

  return (
    <>
      <h3>Time Remaining</h3>
      <div className={`session-timer ${remaining < 60 * 10 ? 'red' : 'white'}`}>
        {remaining_minutes} minutes {remaining_seconds}
        seconds
      </div>
    </>
  );
};

import React from 'react';
import { Link, useParams } from 'react-router-dom';

import { SubmissionView } from './SubmissionView';
import { Submission } from '../types/schema';

type Props = {
  submissions: Submission[];
};

export const SubmissionsView: React.FC<Props> = ({ submissions = [] }) => {
  const { id } = useParams();

  return id ? (
    <SubmissionView />
  ) : (
    <div>
      <h1>All Submissions!</h1>
      {submissions.map((submission, i) => {
        return (
          <div key={i}>
            <Link to={`submissions/${submission.id}`}>{submission.id}</Link>
          </div>
        );
      })}
    </div>
  );
};

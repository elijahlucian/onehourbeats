import React, { useState } from 'react';
import { User } from '../types/schema';

type Props = {
  users: User[];
  login: (userId: string) => void;
};

export const Login: React.FC<Props> = ({ users, login }) => {
  let [userId, setSelectedUser] = useState();

  const selectUser = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedUser(e.target.value);
  };

  const loginWithSelected = () => {
    if (!userId) return;
    window.localStorage.userId = userId;
    login(userId);
  };

  if (window.localStorage.userId) {
    login(window.localStorage.userId);
  }

  return (
    <div>
      <h3>Login As User</h3>
      <select onChange={selectUser} value="default">
        <option disabled value="default">
          Select User!
        </option>
        {users.map(({ id }, i) => {
          return <option key={i}>{id}</option>;
        })}
      </select>
      <button onClick={loginWithSelected}>Login!</button>
    </div>
  );
};

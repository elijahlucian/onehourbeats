import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import _ from 'lodash';

import { submissionIndex } from '../mock_db/submissions';
import { Submission } from '../types/schema';

type Props = {};

export const SubmissionView: React.FC<Props> = () => {
  const [submission, setSubmission] = useState<Submission | undefined>();
  const { id } = useParams();

  useEffect(() => {
    if (!id) return;
    // GET submissions/:id
    // TODO: use string ids
    setSubmission(_.find(submissionIndex, s => s.id === parseInt(id)));
  }, [id]);

  return !submission ? <div>Loading!</div> : <div>{submission.title}</div>;
};

import React from 'react'
import './css/randomizer.css'

type Props = {
  value: number
  i: number
  onClick: () => void
  keyState: boolean
}

export const Randomizer: React.FC<Props> = (props: Props) => {
  return (
    <div className="random" key={props.i}>
      <button value={props.value} onClick={props.onClick}>
        Randomize {props.value}
      </button>
      <div className="randomized">{props.keyState || 'click for random!'}</div>
      <button className="add-button">Add Your Own</button>
    </div>
  )
}

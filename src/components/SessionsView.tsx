import React from 'react';

import { SessionView } from './SessionView';
import { UserSession } from '../types/schema';

import { Link, useRouteMatch, useParams } from 'react-router-dom';

type Props = {
  userSessions: UserSession[];
};

export const SessionsView: React.FC<Props> = ({ userSessions }) => {
  const match = useRouteMatch();
  const { id } = useParams();

  return id ? (
    <SessionView />
  ) : (
    <div>
      <h3>Sessions You Have Jammed In</h3>
      <div>
        {userSessions.map(({ session_id }, i) => (
          <div key={i}>
            <Link to={`${match.url}/${session_id}`}>{session_id}</Link>
          </div>
        ))}
      </div>
    </div>
  );
};

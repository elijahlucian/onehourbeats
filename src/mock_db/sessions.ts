import moment from 'moment';
import { Session } from '../types/schema';

// todo: SessionPrompts will be it's own table

export const sessionIndex: Session[] = [
  {
    id: 'new_jamski_13819',
    created_by: 'basic',
    rules: [0], // denormalize into another table
    prompts: [1, 2],
    submissions: [],

    name: 'New Jam Timer Not Yet Started!',
    description: 'the best jam evar',
    created_at: moment()
      .subtract(1, 'M')
      .unix(),
    verified_only: false, // Milestone 1
    private: false,
    password: undefined, // Milestone 2
    timer_length: 3600,
    timer_started_at: undefined,
  },
  {
    id: 'mumble_rap_circle_23485',
    created_by: 'creator',
    rules: [1],
    prompts: [3, 4],
    submissions: [],

    name: 'Mumble Rap Circle Of Life',
    description: 'the lion king meets bad',
    created_at: moment()
      .subtract(1, 'M')
      .unix(),
    verified_only: false, // Milestone 1
    private: false,
    password: undefined, // Milestone 2
    timer_length: 3600,
    timer_started_at: moment()
      .subtract(50, 'm')
      .subtract(55, 's')
      .unix(),
  },
];

import { Submission } from '../types/schema';

export const submissionIndex: Submission[] = [
  {
    id: 1,
    user_id: 'basic',
    session_id: 'mumble_rap_circle',
    title: 'Mumble Rap Swag',
    description: 'a dank song!',
    votes: 3,
    url: 'dankman-song1.mp3',
  },
  {
    id: 2,
    user_id: 'basic',
    session_id: 'new_jamski_13819',
    title: 'Basic Bit h',
    description: 'some sketch - holla',
    votes: 0,
    url: 'dankman-song2.mp3',
  },
  {
    id: 3,
    user_id: 'creator',
    session_id: 'mumble_rap_circle',
    title: 'My Shit',
    description: 'making the things',
    votes: 45,
    url: 'creator-song1.mp3',
  },
];

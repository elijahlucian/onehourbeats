import { Rule } from '../types/schema';

// generate rules with random ids... (seeded?)

export const rulesIndex: Rule[] = [
  { id: 1, description: 'use all the prompts' },
  { id: 2, description: 'you have to select 2 of the three prompts' },
];

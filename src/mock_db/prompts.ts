import { Prompt } from '../types/schema';

export const promptIndex: Prompt[] = [
  { id: 1, type: 'genres', description: 'indie folk' },
  { id: 2, type: 'lyrics', description: 'you make me high' },
  { id: 3, type: 'genres', description: 'mumble rap' },
  { id: 4, type: 'lyrics', description: 'in the circle of life' },
];

import { User } from '../types/schema';

export const userIndex: User[] = [
  {
    id: 'admin',
    username: '',
    password: '1234',
    created_at: 90210,
    social: '@adminguy',
  },
  {
    id: 'basic',
    username: '',
    password: '1234',
    created_at: 90210,
    social: '@basicbitch',
  },
  {
    id: 'creator',
    username: '',
    password: '1234',
    created_at: 90210,
    social: '@god',
  },
];

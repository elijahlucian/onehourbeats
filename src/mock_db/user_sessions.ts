import { UserSession } from '../types/schema';

export const userSessionIndex: UserSession[] = [
  { user_id: 'basic', session_id: 'new_jamski_13819' },
  { user_id: 'basic', session_id: 'mumble_rap_circle_23485' },
];

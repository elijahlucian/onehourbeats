import _ from 'lodash';

import themes from './themes';
import chordProgressions from './chordProgressions';
import keys from './keys';
import structures from './structures';
import titles from './titles';
import lyrics from './lyrics';
import genres from './genres';
import moods from './moods';

const data = {
  titles,
  themes,
  genres,
  moods,
  lyrics,
  keys,
  chordProgressions,
  structures,
};

const promptTypes: string[] = _.sortBy(Object.keys(data));

export default { ...data, keys: promptTypes };

export default [
  'lydian',
  'myxolydian',
  'ionian',
  'dorian',
  'phyrigian',
  'aeolian',
  'locrian',
]

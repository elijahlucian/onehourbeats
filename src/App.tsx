import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import _ from 'lodash';

import './App.css';
import { Creator } from './components/Creator';
import { Dashboard } from './components/Dashboard';
import { SessionsView } from './components/SessionsView';
import { Sidebar } from './components/Sidebar';
import { Login } from './components/Login';
import { SubmissionsView } from './components/SubmissionsView';

// mock database

import { userIndex } from './mock_db/users';
import { userSessionIndex } from './mock_db/user_sessions';

import { submissionIndex } from './mock_db/submissions';
import { User, Submission, UserSession } from './types/schema';

export default function App() {
  const [user, setUser] = useState<User | null>();
  const [userSubmissions, setUserSubmissions] = useState<Submission[]>([]);
  const [userSessions, setUserSessions] = useState<UserSession[]>([]);

  const login = (user_id: string) => {
    setUser(_.find(userIndex, user => user.id === user_id));
    setUserSubmissions(
      _.filter(submissionIndex, record => record.user_id === user_id),
    );
    setUserSessions(
      _.filter(userSessionIndex, record => record.user_id === user_id),
    );

    // get sessions
    // get submissions here?
    // all submissions or just user's
    // get all active rooms
  };
  if (user) {
    console.log('LOGGED IN WITH', user.id);
  }

  return !user ? (
    <Login login={login} users={userIndex} />
  ) : (
    <Router>
      <nav>
        <Link to="/">Dashboard</Link>
        <Link to="/creator">Create Jam</Link>
      </nav>
      <main>
        <Sidebar
          user={user}
          userSessions={userSessions}
          submissions={userSubmissions}
        />
        <section>
          <Switch>
            <Route path="/creator" component={Creator} />
            <Route path="/sessions/:id?">
              <SessionsView userSessions={userSessions} />
            </Route>
            <Route path="/submissions/:id?">
              <SubmissionsView submissions={userSubmissions} />
            </Route>
            <Route path="/">
              <Dashboard user={user} />
            </Route>
          </Switch>
        </section>
      </main>
      <footer>Status: blah</footer>
    </Router>
  );
}

export enum PromptType {
  titles,
  themes,
  genres,
  moods,
  lyrics,
  keys,
  chordProgressions,
  structures,
}

export type Prompt = {
  id: number;
  type: string;
  description: string;
};

export type Rule = {
  id: number;
  description: string;
};

// Some time-limited session, could be named a 'room'
export type Session = {
  // FK
  id: string;
  created_by: string; // foreign key users
  rules: number[];
  prompts: number[];
  submissions: string[];

  // DATA
  name: string; // checked globally for uniqueness - if private, can be non-unique
  description: string;
  created_at: number;
  timer_started_at?: number;
  timer_length: number;
  verified_only: boolean;
  private: boolean;
  password?: string;
};

export type SessionPrompts = {
  session_id: string;
  prompt_id: string;
};

export type Submission = {
  id: number;
  user_id: string;
  session_id: string;
  title: string;
  description: string;
  votes: number;
  url: string;
};

export type User = {
  id: string;
  username: string;
  password: string;
  created_at: number;
  social: string;
};

export type UserSession = {
  user_id: string;
  session_id: string;
};

export {};
